package com.example.demo.handler;

import com.example.demo.controller.CommandController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class MessageHandlerImpl implements MessageHandler {

    @Autowired
    private CommandController commandController;


    /** Method starts to listen and process all income messages until "exit" command received;
     *
     * @throws IOException
     */
    @Override
    public void listenForMessages() throws IOException {
        String inputMessage;
        commandController.showAllCommands();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (!(inputMessage = reader.readLine()).toLowerCase().equals(EXIT_COMMAND)) {
                commandController.processCommand(recognizeCommand(inputMessage), inputMessage);
            }
        }

    }


    /** Method detect the command and return it's number
     * @param message
     * @return int
     */
    @Override
    public int recognizeCommand(String message) { //
        int result = 0;
        message = message.toLowerCase();
        if (message.startsWith("who is head of department "))
            result = HEAD_OF_DEP_COMMAND;
        if (message.startsWith("show ") && message.endsWith(" statistic"))
            result = DEP_STAT_COMMAND;
        if (message.startsWith("show the average salary for department "))
            result = AVER_SAL__DEP_COMMAND;
        if (message.startsWith("show count of employee for "))
            result = EMPL_COUNT;
        if (message.startsWith("global search by "))
            result = SRCH_COMMAND;
        return result;
    }

}
