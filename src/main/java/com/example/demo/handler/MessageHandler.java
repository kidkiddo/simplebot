package com.example.demo.handler;

import java.io.IOException;

public interface MessageHandler {

    String EXIT_COMMAND = "exit";
    int HEAD_OF_DEP_COMMAND = 1;
    int DEP_STAT_COMMAND = 2;
    int AVER_SAL__DEP_COMMAND = 3;
    int EMPL_COUNT = 4;
    int SRCH_COMMAND = 5;

    /** Method starts to listen and process all income messages until "exit" command received;
     *
     * @throws IOException
     */
    void listenForMessages() throws IOException;

    /** Method detect the command and return it's number
     * @param message
     * @return int
     */
    int recognizeCommand(String message);
}
