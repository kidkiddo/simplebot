package com.example.demo.view;

import com.example.demo.entities.Lector;

import java.util.ArrayList;
import java.util.List;

public interface ResponseView {

    void printResults(String response);

    void printResults(List<Lector> response);

    void printResults(ArrayList<String> response);

    void printResults(int i);
}
