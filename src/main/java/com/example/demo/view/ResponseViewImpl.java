package com.example.demo.view;

import com.example.demo.entities.Lector;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ResponseViewImpl implements ResponseView{

    public void printResults(String response) {
        System.out.println(response);
    }

    @Override
    public void printResults(List<Lector> response) {
        for (Lector lector : response) {
            System.out.println(lector.toString());
        }
    }

    @Override
    public void printResults(ArrayList<String> response) {
        for (String str : response) {
            System.out.println(str);
        }
    }


    @Override
    public void printResults(int i) {
        System.out.println(i);
    }
}
