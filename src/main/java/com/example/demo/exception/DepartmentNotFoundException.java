package com.example.demo.exception;

public class DepartmentNotFoundException extends NullPointerException {

    public DepartmentNotFoundException() {
        super("Department not found!");
    }
}
