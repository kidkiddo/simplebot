package com.example.demo.controller;


import com.example.demo.service.CommandService;
import com.example.demo.view.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


@Controller
public class CommandControllerImpl implements CommandController {

    @Autowired
    private CommandService commandService;

    @Autowired
    private ResponseView responseView;


    /**
     * Depends on command number, this method call the exact Service Method and transfer response to the View part
     *
     * @param command
     * @param message
     */
    @Override
    public void processCommand(int command, String message) {
        switch (command) {
            case 0:
                showAllCommands();
                break;
            case 1:
                responseView.printResults(commandService.showHeadOfDepartment(parseData(command, message)));
                break;
            case 2:
                responseView.printResults(commandService.showDepartmentStatistic(parseData(command, message)));
                break;
            case 3:
                responseView.printResults(commandService.showAverageSalaryForDepartment(parseData(command, message)));
                break;
            case 4:
                responseView.printResults(commandService.showCountOfEmployee(parseData(command, message)));
                break;
            case 5:
                responseView.printResults(commandService.searchByKeyWord(parseData(command, message)));
                break;
        }
    }


    /**
     * Print all available commands. Hardcoded in CommandController
     */
    @Override
    public void showAllCommands() {
        System.out.println(commandsAvailable);
    }


    /** Extracting keyword from command, depending on the method
     * @param command
     * @param message
     * @return
     */
    private String parseData(int command, String message) {
        String result = "";
        switch (command) {
            case 1:
                result = message.substring(message.indexOf("of department ") + 14, message.length());
                break;
            case 2:
                result = message.substring(message.indexOf("show ") + 5, message.indexOf(" statistic"));
                break;
            case 3:
                result = message.substring(message.indexOf("for department ") + 15, message.length());
                break;
            case 4:
                result = message.substring(message.indexOf("employee for ") + 13, message.length());
                break;
            case 5:
                result = message.substring(message.indexOf("by ") + 3, message.length());
                break;
        }
        return result;
    }

}
