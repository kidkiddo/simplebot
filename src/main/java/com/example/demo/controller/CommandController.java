package com.example.demo.controller;

public interface CommandController {
    String commandsAvailable = "Commands : \n1. Who is head of department {department_name}  \n" +
            "2. Show {department_name} statistic. \n" +
            "3. Show the average salary for department {department_name}. \n" +
            "4. Show count of employee for {department_name}. \n" +
            "5. Global search by {template}. \n" +
            "6. Exit \n";


    /**
     * Depends on command number, this method call the exact Service Method and transfer response to the View part
     *
     * @param command
     * @param message
     */
    void processCommand(int command, String message);


    /**
     * Print all available commands.
     */
    void showAllCommands();
}
