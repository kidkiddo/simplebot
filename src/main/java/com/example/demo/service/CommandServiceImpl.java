package com.example.demo.service;

import com.example.demo.dao.CommandDAO;
import com.example.demo.entities.Department;
import com.example.demo.entities.Lector;
import com.example.demo.exception.DepartmentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommandServiceImpl implements CommandService {

    @Autowired
    private CommandDAO dao;


    /**
     * Using DAO layer, service extract requested department by name. Getting Lector from "head" var and return it's name
     *
     * @param departmentName
     * @return Lector "name" var
     */
    @Override
    public String showHeadOfDepartment(String departmentName) {
        String template = "Head of %s department is %s";
        Lector lector = null;
        Department department = null;
        try {
            department = dao.getDepartmentByName(departmentName);
        } catch (DepartmentNotFoundException e) {
            return "Department not found!";
        }
        lector = department.getHead();
        if (lector == null) {
            return "No head of department found";
        }
        return (String.format(template, departmentName, lector.getName()));
    }


    /** Extract requested department name. Extract all Lectors from Department and count each by degree value
     * @param departmentName
     * @return ArrayList with name of degree and count
     */
    @Override
    @Transactional
    public ArrayList<String> showDepartmentStatistic(String departmentName) {
        int assistantsCount = 0;
        int associateProffessorsCount = 0;
        int professorsCount = 0;
        List<Lector> lectors = null;
        ArrayList<String> result = new ArrayList<>();
        Department department = null;
        try {
            department = dao.getDepartmentByName(departmentName);
        }
        catch (DepartmentNotFoundException e) {
            result.add("Department not found!");
            return result;
        }
        lectors = department.getLectors();
        for (Lector lector : lectors) {
            String degree = lector.getDegree().toLowerCase();
            if (degree.equals("assistant")) {
                assistantsCount++;
            } else if (degree.equals("associate professor")) {
                associateProffessorsCount++;
            } else if (degree.equals("professor")) {
                professorsCount++;
            }
        }
        result.add("assistants - " + assistantsCount);
        result.add("associate professors - " + associateProffessorsCount);
        result.add("professors - " + professorsCount);
        return result;
    }


    /** Method gets total department salary, total employees and calculate the average salary
     * @param departmentName
     * @return Department name and it's average salary
     */
    @Override
    @Transactional
    public String showAverageSalaryForDepartment(String departmentName) {
        String template = "The average salary of %s is %.1f";
        double averageSalary = 0.d;
        double totalSalary = 0.0d;
        int employeeCount = 0;

        totalSalary = dao.countTotalSalaryInDepartment(departmentName);
        employeeCount = dao.countLectorsInDepartment(departmentName);
        averageSalary = (totalSalary / employeeCount);
        return (String.format(template, departmentName, averageSalary));
    }


    /** Count of employee by Department
     * @param departmentName
     * @return count of employee
     */
    @Override
    @Transactional
    public String showCountOfEmployee(String departmentName) {
        String result = String.valueOf(dao.countLectorsInDepartment(departmentName));
        return result;
    }


    /**
     * @param keyword
     * @return List of Lectors that has similar to keyword char combination in it's vars
     */
    @Override
    public List<Lector> searchByKeyWord(String keyword) {
        List<Lector> lectors = dao.search(keyword);
        return lectors;
    }
}
