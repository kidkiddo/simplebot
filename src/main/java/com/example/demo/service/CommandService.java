package com.example.demo.service;

import com.example.demo.entities.Lector;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public interface CommandService {

    /**
     * Using DAO layer, service extract requested department by name. Getting Lector from "head" value and return it's name var
     *
     * @param departmentName
     * @return Lector "name" var
     */
    String showHeadOfDepartment(String departmentName);

    /** Extract requested department name. Extract all Lectors from Department and count each by degree value
     * @param departmentName
     * @return ArrayList with name of degree and count
     */
    @Transactional
    ArrayList<String> showDepartmentStatistic(String departmentName);

    /** Method gets total department salary, total employees and calculate the average salary
     * @param departmentName
     * @return Department name and it's average salary
     */
    @Transactional
    String showAverageSalaryForDepartment(String departmentName);

    /** Count of employee by Department
     * @param departmentName
     * @return count of employee
     */
    @Transactional
    String showCountOfEmployee(String departmentName);

    /**
     * @param keyword
     * @return List of Lectors that has similar to keyword char combination in it's vars
     */
    List<Lector> searchByKeyWord(String keyword);
}
