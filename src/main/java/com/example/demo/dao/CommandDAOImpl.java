package com.example.demo.dao;

import com.example.demo.entities.Department;
import com.example.demo.entities.Lector;
import com.example.demo.exception.DepartmentNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional(noRollbackForClassName = "com.example.demo.exception.DepartmentNotFound")
public class CommandDAOImpl implements CommandDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public static final String TOTAL_EMPLOYEE_BY_DEP_COUNT = "SELECT count(lector_id) FROM department_lector where department_name='%s'";
    public static final String TOTAL_SALARY_BY_DEP_COUNT = "SELECT  sum(l.salary) as salary from lectors as l inner join department_lector as dl on dl.lector_id = l.id where dl.department_name='%s' group by department_name";

    /**
     * @param departmentName
     * @return Department by primary key
     * @throws DepartmentNotFoundException
     */
    @Override
    public Department getDepartmentByName(String departmentName) throws DepartmentNotFoundException{
        Session session = sessionFactory.getCurrentSession();
        Department department = session.get(Department.class, departmentName);
        if (department == null) {
            throw new DepartmentNotFoundException();
        }
        return department;
    }

    /**
     * @param keyword
     * @return List of Lectors that have similar to keyword data in it's fields
     */
    @Override
    public List<Lector> search(String keyword) { // I decide to use more simple and reliable solution that pass here better on my opinion, instead of using Hibernate Lucene
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Lector where lower(name) like :keyword or lower(degree) like :keyword or lower(salary) like :keyword");
        query.setParameter("keyword", "%" + keyword.toLowerCase() + "%");
        List<Lector> lectors = query.getResultList();
        return lectors;
    }


    /**
     * @param departmentName
     * @return count of lectors in exact Department
     */
    public int countLectorsInDepartment(String departmentName) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createNativeQuery(String.format(TOTAL_EMPLOYEE_BY_DEP_COUNT, departmentName));
        return Integer.parseInt(String.valueOf(query.getSingleResult()));
    }

    /**
     * @param departmentName
     * @return summ of salary in exact Department
     */
    @Override
    public double countTotalSalaryInDepartment(String departmentName) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createNativeQuery(String.format(TOTAL_SALARY_BY_DEP_COUNT, departmentName));
        return (double) query.getSingleResult();
    }

}
