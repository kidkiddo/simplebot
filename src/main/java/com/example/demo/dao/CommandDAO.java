package com.example.demo.dao;

import com.example.demo.entities.Department;
import com.example.demo.entities.Lector;
import com.example.demo.exception.DepartmentNotFoundException;

import java.util.List;

public interface CommandDAO {

    int countLectorsInDepartment(String departmentName);

    double countTotalSalaryInDepartment(String departmentName);


    /**
     * @param departmentName
     * @return Department by primary key
     * @throws DepartmentNotFoundException
     */
    Department getDepartmentByName(String departmentName);


    /**
     * @param keyword
     * @return List of Lectors that have similar to keyword data in it's fields
     */
    List<Lector> search(String keyword);


}
